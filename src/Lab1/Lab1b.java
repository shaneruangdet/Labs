/*
 * Lab1b.java
 *
 * This program produces the song output detailed in CSSSKL142 Lab 1 Part 3.
 * Authors: Samantha Smith, you
 */

public class Lab1b {
    public static void main(String[] args) {
        int numBottles; // The bottles currently on the wall

        // Verse 1
        numBottles = 5;
        System.out.print(numBottles);
        onWall();
        System.out.print(numBottles);
        botBeer();
        takeOneDown();
        numBottles = 4; // Update numBottles after taking one down
        System.out.print(numBottles);
        onWall();
        System.out.println(); // Blank line

        // Verse 2
        numBottles = 4;
        System.out.print(numBottles);
        onWall();
        System.out.print(numBottles);
        botBeer();
        takeOneDown();
        numBottles = 3; // Update numBottles after taking one down
        System.out.print(numBottles);
        onWall();
        System.out.println(); // Blank line

        // Verse 3
        numBottles = 3;
        System.out.print(numBottles);
        onWall();
        System.out.print(numBottles);
        botBeer();
        takeOneDown();
        numBottles = 2; // Update numBottles after taking one down
        System.out.print(numBottles);
        onWall();
        System.out.println(); // Blank line

        // Verse 4
        numBottles = 2;
        System.out.print(numBottles);
        onWall();
        System.out.print(numBottles);
        botBeer();
        takeOneDown();
        numBottles = 1; // Update numBottles after taking one down
        System.out.print(numBottles);
        onWall();
        System.out.println(); // Blank line

        // Verse 5
        numBottles = 1;
        System.out.print(numBottles);
        onWall();
        System.out.print(numBottles);
        botBeer();
        takeOneDown();
        numBottles = 0; // Update numBottles after taking one down
        System.out.println("No bottles of beer on the wall");
        System.out.println(); // Blank line
        // TODO: Fill in the other verses
    }

    /**
     * onWall
     * <p>
     * Print the unchanging element of the line "<numBottle> bottles of beer on the wall"
     */
    public static void onWall() {

        System.out.println(" bottles of beer on the wall");
    }

    /**
     * botBeer
     * <p>
     * Print the unchanging element of the line "<numBottle> bottles of beer"
     */
    public static void botBeer() {

        System.out.println(" bottles of beer");
    }

    /**
     * takeOneDown
     * <p>
     * Print the line "Take one down and pass it around"
     */
    // TODO: You figure out the declaration and the method!
    public static void takeOneDown() {
        System.out.println("Take one down and pass it around");
    }
}
