import java.util.Scanner;

/*
 * Lab2a.java
 *
 * Authors: Samantha Smith, you
 */
// TODO: Import Scanner

public class Lab2a {
    public static void main(String[] args){
        // kelvins and temperaturePrinter tests
        System.out.println("\nTesting kelvins and temperaturePrinter:");
        double f1 = 35;
        temperaturePrinter(f1, kelvins(f1));

        double f2 = 40;
        temperaturePrinter(f2, kelvins(f2));

        double f3 = 48;
        temperaturePrinter(f3, kelvins(f3));

        double f4 = 70;
        temperaturePrinter(f4, kelvins(f4));

        double f5 = 99;
        temperaturePrinter(f5, kelvins(f5));
        // TODO: Add at least five tests

        // inSeconds tests
        System.out.println("\nTesting inSeconds:");
        inSeconds(1, 5, 1); // Expect: 3901
        inSeconds(4, 7, 3);
        inSeconds(5, 9, 2);
        inSeconds(3, 5, 8);
        inSeconds(2, 8, 1);
        // TODO: Add at least 5 tests

        // secondTime tests
        System.out.println("\nTesting secondTime:");
        // TODO: Declare a scanner

        System.out.println("Please input total seconds as an integer");
        Scanner sc = new Scanner(System.in);
        int val = sc.nextInt();            
        secondTime(val);
    }

    /**
     * kelvins
     *
     * This method converts a value in F to the corresponding value in K.
     *
     * @param   f   double  The temperature in Fahrenheit
     * @return  The temperature in Kelvins
     */
    public static double kelvins(double f) {
    double k = 5.0 / 9 * (f - 32) + 273.15;
    return k;
    }

    /**
     * temperaturePrinter
     *
     * This method prints the message "<f> F corresponds to <k> K"
     *
     * @param   f   double  The temperature in Fahrenheit
     * @param   k   double  The temperature in Kelvins
     */
    public static void temperaturePrinter(double f, double k) {
        System.out.printf("%.1f°F corresponds to %.2f K\n", f, k);
    }



    /**
     * secondTime
     *
     * This method converts from seconds to hour:minutes:seconds, and prints the
     * result to the console.
     *
     * @param   inputSeconds    int The total seconds to convert
     */
    // TODO: You figure out the declaration and the method!
    public static void secondTime(int seconds){
        int hr = seconds / 3600;
        int hrRemainder = seconds % 3600;

        int min = hrRemainder / 60;
        int minRemainder = hrRemainder % 60;

        int sec = minRemainder / 60;
        int secRemainder = hrRemainder % 60;

        System.out.println(seconds + " seconds corresponds to: " +
                hr + " hours " +
                min + " minutes " +
                secRemainder + " seconds");
    }
    /**
     * inSeconds
     *
     * This method converts from hours:minutes:seconds to seconds, and prints the
     * result to the console.
     *
     * @param   hours   int The hours to convert
     * @param   mins    int The minutes to convert
     * @param   secs    int The seconds to convert
     * @return The total number of seconds
     */
    // TODO: You figure out the declaration and the method!
    public static void inSeconds(int a, int b, int c) {
        int hours = a * 3600;
        int mins = b * 60;
        int seconds = c;
        System.out.println(hours + mins + seconds);

    }

}
