/*
 * Lab2b.java
 *
 * Authors: Samantha Smith, you
 */

// TODO: Do you need any imports?

import java.util.Scanner;

public class Lab2b {
	public static void main(String[] args) {
		// TODO: Declare a Scanner
		System.out.println("Please enter you name followed by three numbers (space separated): ");
		Scanner keyboard = new Scanner(System.in);
		String name = keyboard.next();
		double input = keyboard.nextDouble();
		double input2 = keyboard.nextDouble();
		double input3 = keyboard.nextDouble();
		System.out.println("Hi there, " + name + "! Here are the numbers you entered in descending order:");
		sortDescending(input, input2, input3);
		System.out.println("Thank you!");
	}
	public static void sortDescending(double a, double b, double c) {
		double min    = Math.min(a, Math.min(b, c));
		double max    = Math.max(a, Math.max(b, c));
		double median = a + b + c - min - max; // Had to look up on the internet on how to do this line!
		System.out.println(max + " " + median + " and " + min);
	}
		// TODO: Request and parse user input

		// TODO: Sort values and display

		// TODO: Thank user
}

	// Declare a sortDescending method that will take three doubles (optional)

